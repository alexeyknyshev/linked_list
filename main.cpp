#include <iostream>

#include "linkedlist.h"

using namespace std;

int main()
{
    LinkedList<int> intList;

    for (int i = 0; i < 10; ++i)
    {
        intList.pushBack(i + 1);
        cout << "intList.pushBack(" << i + 1 << ")" << endl;
    }

    cout << "intList.size() == " << intList.size() << endl;

    cout << "intList.at(2) == " << intList.at(2) << endl;

    cout << endl;
    cout << "-----------------------------------" << endl;
    cout << endl;

    intList.erase(2);
    cout << "intList.erase(2)" << endl;

    {
        const int size = intList.size();
        for (int i = 0; i < size; ++i)
        {
            cout << "intList.at(" << i << ") == " << intList.at(i) << endl;
        }
    }

    cout << endl;
    cout << "-----------------------------------" << endl;
    cout << endl;

    cout << "intList.clear()" << endl;
    intList.clear();

    cout << "intList.size() == " << intList.size() << endl;

    cout << endl;
    cout << "-----------------------------------" << endl;
    cout << endl;

    for (int i = 0; i < 10; i++)
    {
        intList.pushFront(i + 1);
        cout << "intList.pushBack(" << i + 1 << ")" << endl;
    }

    cout << endl;

    {
        const int size = intList.size();
        for (int i = 0; i < size; ++i)
        {
            cout << "intList.at(" << i << ") == " << intList.at(i) << endl;
        }
    }

    cout << "intList.clear()" << endl;
    intList.clear();;

    cout << endl;
    cout << "-----------------------------------" << endl;
    cout << endl;

    intList.pushBack(3);
    cout << "intList.pushBack(3)" << endl;

    intList.pushFront(1);
    cout << "intList.pushFront(1)" << endl;

    intList.pushBack(4);
    cout << "intList.pushBack(4)" << endl;

    intList.pushFront(0);
    cout << "intList.pushFront(0)" << endl;

    {
        const int size = intList.size();
        for (int i = 0; i < size; ++i)
        {
            cout << "intList.at(" << i << ") == " << intList.at(i) << endl;
        }
    }

    cout << endl;
    cout << "-----------------------------------" << endl;
    cout << endl;

    intList.insert(2, 2);
    cout << "intList.insert(2, 2)" << endl;

    {
        const int size = intList.size();
        for (int i = 0; i < size; ++i)
        {
            cout << "intList.at(" << i << ") == " << intList.at(i) << endl;
        }
    }

    cout << "intList.clear()" << endl;
    intList.clear();;

    cout << "intList.size() == " << intList.size() << endl;

    cout << endl;
    cout << "-----------------------------------" << endl;
    cout << endl;


    intList.insert(10, 3);
    cout << "intList.insert(10, 3)" << endl;

    intList.insert(11, 4);
    cout << "intList.insert(11, 4)" << endl;

    intList.insert(12, 5);
    cout << "intList.insert(12, 5)" << endl;

    intList.insert(0, 2);
    cout << "intList.insert(0, 2)" << endl;

    intList.insert(-1, 1);
    cout << "intList.insert(-1, 1)" << endl;

    intList.insert(0, 0);
    cout << "intList.insert(0, 0)" << endl;

    {
        const int size = intList.size();
        for (int i = 0; i < size; ++i)
        {
            cout << "intList.at(" << i << ") == " << intList.at(i) << endl;
        }
    }

    return 0;
}

