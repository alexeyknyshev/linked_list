#include <assert.h>

template<typename T>
class LinkedList
{
public:
    LinkedList()
        : mHead(nullptr)
    { }

    // ---------------------------------

    virtual ~LinkedList()
    {
        clear();
    }

    // ---------------------------------

    void pushBack(T val)
    {
        Node<T> *newNode = new Node<T>;
        newNode->data = val;

        if (mHead)
        {
            Node<T> *node = mHead;
            while (node->mNext)
            {
                node = node->mNext;
            }

            node->mNext = newNode;
        }
        else
        {
            mHead = newNode;
        }
    }

    // ---------------------------------

    void pushFront(T val)
    {
        Node<T> *newNode = new Node<T>;
        newNode->data = val;
        
        newNode->mNext = mHead;
        mHead = newNode;
    }

    // ---------------------------------

    void insert(int index, T val)
    {
        if (index <= 0 || empty())
        {
            pushFront(val);
            return;
        }

        Node<T> *preNode = mHead;
        while (--index && preNode->mNext)
        {
            preNode = preNode->mNext;
        }

        Node<T> *newNode = new Node<T>;
        newNode->data = val;
        newNode->mNext = preNode->mNext;

        preNode->mNext = newNode;
    }

    // ---------------------------------

    T &at(int index)
    {
        assert(index >= 0 && "invalid negative index!");

        static T BLANK;

        if (!mHead)
        {
            assert(false && "list is empty!");
            return BLANK;
        }

        Node<T> *indexedNode = mHead;
        while (index--)
        {
            if (indexedNode->mNext)
            {
                indexedNode = indexedNode->mNext;
            }
            else
            {
                assert(false && "invalid out of range index!");
                return BLANK;
            }
        }
        return indexedNode->data;
    }

    // ---------------------------------

    void erase(int index)
    {
        assert(index >= 0 && "invalid negative index!");

        if (!mHead)
        {
            assert(false && "list is empty!");
        }

        if (index == 0)
        {
            Node<T> *newHead = mHead->mNext;
            delete mHead;
            mHead = newHead;

            return;
        }

        Node<T> *preNode = mHead;
        int preNodeIndex = index - 1;

        while (preNodeIndex--)
        {
            if (preNode->mNext)
            {
                preNode = preNode->mNext;
            }
            else
            {
                assert(false && "invalid out of range index!");
            }
        }

        Node<T> *eraseNode = preNode->mNext;
        if (!eraseNode)
        {
            assert(false && "invalid out of reange index!");
            return;
        }

        preNode->mNext = eraseNode->mNext;
        delete eraseNode;
    }

    // ---------------------------------

    void clear()
    {
        Node<T> *eraseNode = mHead;
        while (eraseNode)
        {
            Node<T> *nextEraseNode = eraseNode->mNext;
            delete eraseNode;
            eraseNode = nextEraseNode;
        }

        mHead = nullptr;
    }

    // ---------------------------------

    int size() const
    {
        int count = 0;
        Node<T> *node = mHead;
        while (node)
        {
            count++;
            node = node->mNext;
        }

        return count;
    }

    // ---------------------------------

    inline bool empty() const
    {
        return !mHead;
    }

private:
    template<typename Type>
    class Node
    {
        friend class LinkedList;

        Type data;
        Node<Type> *mNext;

    public:
        Node()
            : mNext(nullptr)
        { }
    };

    Node<T> *mHead;
};
